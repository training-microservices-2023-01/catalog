# Cara build docker image #

1. Build dulu aplikasinya

    ```
    mvn clean package -DskipTests
    ```

    Pastikan ada file `catalog-0.0.1-SNAPSHOT.jar` di dalam folder `target`

2. Jalankan docker build

    ```
    docker build -t catalog .
    ```

    **Jangan lupa titik `.` di paling belakang**

3. Jalankan docker image yang barusan dibuat

    ```
    docker run -e SPRING_DATASOURCE_URL=jdbc:postgresql://192.168.79.82:54322/catalogdb -e SPRING_R2DBC_URL=r2dbc:postgresql://192.168.79.82:54322/catalogdb -p 10001:8080 catalog
    ```

    * `-e` : memasang environment variabel ke dalam container
    * `-p` : export/publish port `8080` dalam container ke port `10001` di komputer host

## Publish image ke dockerhub ##

1. Login dulu dengan akun dockerhub

    ```
    docker login
    ```

2. Tag image dengan memberikan prefix username

    ```
    docker image tag catalog endymuhardin/catalog
    docker image tag catalog endymuhardin/catalog:0.0.1-SNAPSHOT
    ```

3. Push (upload) image

    ```
    docker image push --all-tags endymuhardin/catalog
    ```

4. Cek hasilnya di dockerhub

[![Docker Hub Image List](img/docker-hub-image-list.png)](img/docker-hub-image-list.png)

[![Docker Hub Image Detail](img/docker-hub-image-detail.png)](img/docker-hub-image-detail.png)