package com.muhardin.endy.training.microservices.catalog.controller;

import java.net.InetSocketAddress;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import com.muhardin.endy.training.microservices.catalog.dao.ProdukDao;
import com.muhardin.endy.training.microservices.catalog.entity.Produk;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RestController @RequestMapping("/api")
public class ProdukController {
    @Autowired private ProdukDao produkDao;

    @GetMapping("/produk/")
    public Flux<Produk> dataProduk(){
        log.info("Menjalankan method list product");
        return produkDao.findAll();
    }

    @GetMapping("/produk/{id}")
    public Mono<Produk> cariById(@PathVariable String id){
        return produkDao.findById(id);
    }

    @GetMapping("/info")
    public Map<String, Object> backendInfo(ServerWebExchange exchange, Authentication currentUser){
        log.info("Menjalankan method info backend");
        Map<String, Object> info = new HashMap<>();
        InetSocketAddress local = exchange.getRequest().getLocalAddress();
        if(local != null) {
            info.put("hostname", local.getHostName());
            info.put("ip-server", local.toString());
            info.put("port", local.getPort());
            info.put("waktu", LocalDateTime.now());
            info.put("currentUserObject", currentUser);
            info.put("currentUsername", currentUser.getName());
        }
        return info;
    }
}
