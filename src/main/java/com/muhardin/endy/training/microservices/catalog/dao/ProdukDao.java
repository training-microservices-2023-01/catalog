package com.muhardin.endy.training.microservices.catalog.dao;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.muhardin.endy.training.microservices.catalog.entity.Produk;

public interface ProdukDao extends ReactiveCrudRepository<Produk, String> {
    
}
