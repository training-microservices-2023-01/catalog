package com.muhardin.endy.training.microservices.catalog.entity;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Produk {

    @Id
    private String id;
    private String kode;
    private String nama;
    private BigDecimal harga;
}
